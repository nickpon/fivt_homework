﻿/* Пономаренко Николай 691
Найдите все вхождения шаблона в строку.
Найдите все вхождения шаблона в строку. Длина шаблона – p, длина строки – n. Время O(n + p), доп. память – O(p).
Вариант 1. С помощью префикс-функции;
Вариант 2. С помощью z-функции.
p <= 30000, n <= 300000.
*/

#include <iostream>
#include <string>
#include <vector>

class StringSearch {
public:
    StringSearch() {}; //deafault inisializer
    StringSearch(std::string _needle); //inisializer
    std::vector<int> Prefixs(std::string s); // Finds out array of prefixes for the string given
    std::vector <int> KMP(); // Knut-Morrison-Prat algorithm
    void PrintAnswerKMP(); // Prints answer for KMP algorithm
private:
    std::string needle; // pattern that is needed to be found (analogy with a needle in a haystack)
};

StringSearch::StringSearch(std::string _needle) {
    needle = _needle;
}

std::vector<int> StringSearch::Prefixs(std::string s) {
    std::vector <int> pi(s.length()); // array for Prefix function
    for (int i = 1; i < s.length(); ++i) { // pi[0] = 0 (we define it this way); pefix function algorithm
        int j = pi[i - 1];
        while ((j > 0) && (s[j] != s[i])) j = pi[j - 1];
        if (s[j] == s[i]) ++j;
        pi[i] = j;
    }
    return pi;
}

std::vector <int> StringSearch::KMP() {
    int index = -1; // index that is currently searched
    int NeedleLength = needle.length();
    std::vector <int> ans; // array of answers
    std::vector <int> f; // array for prefixes base on the needle string
    f = Prefixs(needle);
    int k = 0;
    char symbol; // symbol in the haystack string
    int  i = 0; // shows the position of each symbol in haystack string
    while (std::cin >> symbol) { // similar to Prefix function algorith
        while ((k > 0) && (needle[k] != symbol)) k = f[k - 1];
        if (needle[k] == symbol) ++k;
        if (k == NeedleLength) {
            index = i - NeedleLength + 1; // counts the index
            ans.push_back(index);
        }
        ++i;
    }
    return ans;
}

void StringSearch::PrintAnswerKMP() {
    std::vector <int> ans;
    ans = KMP();
    for (auto i : ans)
        std::cout << i << " ";
}

int main() {
    std::string needle;
    std::cin >> needle;
    StringSearch MyClass(needle);
    MyClass.PrintAnswerKMP();
    return 0;
}